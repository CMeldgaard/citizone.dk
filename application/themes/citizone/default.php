<?php include 'includes/header.php';?>
<body>
	
	<div class="container global-wrap">
		<?php include 'includes/menu.php';?>
		<div class="gap"></div>
		<div class="container">
			<div class="col-md-10 col-md-offset-1">
				<div class="text-center">
					<?php $a = new Area('Headline');
					$a->display($c); ?>
				</div>
				<div class="gap"></div>
				<?php $a = new Area('Content');
					$a->display($c); ?>
			</div>
		</div>
		<div class="gap"></div>
	<?php include 'includes/bottom.php';?>	