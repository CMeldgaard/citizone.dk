<?php include 'includes/header.php';?>
<body>
	
	<div class="container global-wrap">
		<?php include 'includes/menu.php';?>
		<div class="gap"></div>
		<div class="container">
			<div class="col-md-10 col-md-offset-1">
				<div class="text-center">
					<?php $a = new Area('Headline');
					$a->display($c); ?>
				</div>
			</div>
		</div>
		<div class="gap"></div>
		<div class="container">
            <div class="row">
                <div class="col-md-8 r-mb20">
                    <?php $a = new Area('About Slider');
					$a->display($c); ?>
				</div>
                <div class="col-md-4">
					<?php $a = new Area('Virksomhed');
					$a->display($c); ?>
				</div>
			</div>
            <div class="gap"></div>
            <div class="row">
                <div class="col-md-4">
					<?php $a = new Area('Vaerdier');
					$a->display($c); ?>
                    
				</div>
                <div class="col-md-4">
                    <?php $a = new Area('Mission');
					$a->display($c); ?>
				</div>
                <div class="col-md-4">
                    <?php $a = new Area('Vision');
					$a->display($c); ?>
				</div>
			</div>
		</div>
		<div class="gap"></div>
		<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php $a = new Area('Team header');
						$a->display($c); ?>
			</div>
			<div class="col-md-3 r-mb20">
			<?php $a = new Area('Team image');
						$a->display($c); ?>
			</div>
			<div class="col-md-6 r-mb20">
			<?php $a = new Area('Team text');
						$a->display($c); ?>
			</div>
			<div class="col-md-3 r-mb20">
			<?php $a = new Area('Team exp');
						$a->display($c); ?>
			</div>
		</div>
		</div>
		<div class="gap"></div>
	<?php include 'includes/bottom.php';?>	