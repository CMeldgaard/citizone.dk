<div class="top-main-area">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-xs-7">
							<a href="/" class="logo">
								<img src="<?php echo $this->getThemePath(); ?>/images/logo.png" alt="CitiZone's Logo" title="CitiZone's Logo">
							</a>
						</div>
						<div class="col-md-8 col-md-offset-1 col-xs-5">
							<div class="pull-right">
								<ul class="header-features hidden-sm hidden-xs">
									<li><i class="fa fa-phone"></i>
										<div class="header-feature-caption">
											<h5 class="header-feature-title">+45 2045 6083</h5>
											<p class="header-feature-sub-title">Åbent alle hverdage 8-16</p>
										</div>
									</li>
									<li><i class="fa fa-file-text-o"></i>
										<div class="header-feature-caption">
											<h5 class="header-feature-title">Dokumentation</h5>
											<p class="header-feature-sub-title">på alle vores produkter</p>
										</div>
									</li>
									<li><i class="fa fa-leaf"></i>
										<div class="header-feature-caption">
											<h5 class="header-feature-title">Miljøvenlige</h5>
											<p class="header-feature-sub-title">løsninger</p>
										</div>
									</li>
								</ul>
								
								<ul class="header-features hidden-md hidden-lg">
								<?php
						global $u;
						if ($u -> isLoggedIn ()) {?>
						<li><a class="" href="/account"><i class="fa fa-user"></i></a>
						</li>
						<li><a class="" href="<?php echo URL::to('/login', 'logout', Loader::helper('validation/token')->generate('logout'))?>"><i class="fa fa-sign-out"></i></a>
						</li>
						<?php
							}
						else
						{
						?>
						<li><a class="popup-text" href="#login-dialog" data-effect="mfp-fade"><i class="fa fa-sign-in"></i></a>
						</li>
						<li><a class="popup-text" href="#sign-up-dialog" data-effect="mfp-fade"><i class="fa fa-user-plus"></i></a>
							</li>
						<?php
							}
						?>
									
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<header class="main bgColor container">
				<div class="row">
                    <div class="col-md-9 pl0 pr0">
                        <div class="flexnav-menu-button" id="flexnav-menu-button">Menu<span class="touch-button"></span></div>
						<nav>
                        <?php 
								$a = new GlobalArea('Navigation');
								$a->display();
								?>
						</nav>
					</div>
                    <div class="hidden-xs hidden-sm col-md-3">
                        <ul class="login-register">
						<?php
						global $u;
						if ($u -> isLoggedIn ()) {?>
						<li class="hidden-xs"><a class="" href="/account"><i class="fa fa-user"></i>Min Profil</a>
						</li>
						<li class="hidden-xs"><a class="" href="<?php echo URL::to('/login', 'logout', Loader::helper('validation/token')->generate('logout'))?>"><i class="fa fa-sign-out"></i>Log ud</a>
						</li>
						<?php
							}
						else
						{
						?>
						<li class="hidden-xs"><a class="popup-text" href="#login-dialog" data-effect="mfp-fade"><i class="fa fa-sign-in"></i>Log ind</a>
							</li>
						<li class="hidden-xs"><a class="popup-text" href="#sign-up-dialog" data-effect="mfp-fade"><i class="fa fa-user-plus"></i>Registrer</a>
							</li>
						<?php
							}
						?>
						</ul>
					</div>
				</div>
			</header>