
<div id="login-dialog" class="mfp-with-anim mfp-dialog clearfix mfp-hide">
	<i class="fa fa-sign-in dialog-icon"></i>
	<h3>Kunde log ind</h3>
	<h5>Velkommen tilbage. Log ind for at få adgang til yderligere informationer</h5>
	<?php
		use Concrete\Core\Attribute\Key\Key;
		use Concrete\Core\Http\ResponseAssetGroup;
		
		defined('C5_EXECUTE') or die('Access denied.');
		
		$r = ResponseAssetGroup::get();
		$r->requireAsset('javascript', 'underscore');
		$r->requireAsset('javascript', 'core/events');
		$r->requireAsset('core/legacy');
		
		$activeAuths = AuthenticationType::getList(true, true);
		$form = Loader::helper('form');
		$token = Loader::helper('validation/token')->generate('login_concrete');
		
		$active = null;
		if ($authType) {
			$active = $authType;
			$activeAuths = array($authType);
		}
		
		
		/** @var Key[] $required_attributes */
		
		$attribute_mode = (isset($required_attributes) && count($required_attributes));
	?>
	<?php
		/** @var AuthenticationType[] $activeAuths */
		
		foreach ($activeAuths as $auth) {
		?>
		<div data-handle="<?php echo $auth->getAuthenticationTypeHandle() ?>" class="authentication-type authentication-type-<?php echo $auth->getAuthenticationTypeHandle() ?>">
			<form method="post" action="" id="loginForm">
			<div class="alert alert-danger"id="loginError" role="alert" style="display: none;">Der er fejl i de indtastede data</div>
				<div class="form-group">
					<?php print $form->text('uName', '', array('style' => 'width: 100%', 'tabindex' => 2, 'placeholder' => 'E-mail'))?>
				</div>
				<div class="form-group">
					<?php print $form->password('uPassword', '', array('style' => 'width: 100%', 'tabindex' => 2, 'placeholder' => 'Password'))?>
				</div>
				<div class="checkbox">
					<label style="font-weight:normal">
						<?php print $form->checkbox('uMaintainLogin', 1, 0, '')?>
					Forbliv logget ind i to uger        </label>
				</div>
				<input type="hidden" name="ccm_token" value="<?php print $token?>">
				<div class="form-group">
					<button class="btn btn-primary">Log ind</button> <img src="<?php echo $this->getThemePath(); ?>/images/loader.gif" id="ajaxLoader" style="width: 24px; height: 24px; display: none;">
				</div>
				<script type="text/javascript">
					document.querySelector('input[name=uName]').focus();
					
					$("#loginForm").submit(function() {
						$("#ajaxLoader").show();
						$("#loginError").hide();
						var url = "/index.php/login/authenticate/concrete"; // the script where you handle the form input.

						$.ajax({
							   type: "POST",
							   url: url,
							   data: $("#loginForm").serialize(), // serializes the form's elements.
							   success: function(data)
							   {
								   if (data.indexOf("<title>CitiZone :: Login</title>") < 337){
								     $("#ajaxLoader").hide();
									 $("#loginError").show();
								   }else{
								    location.reload();
								  };
							   }
							 });

						return false; // avoid to execute the actual submit of the form.
					});
				</script>
			</form>
		</div>
		<?php
		}
	?>
	<ul class="dialog-alt-links">
		<li><a class="popup-text" href="#password-recover-dialog" data-effect="mfp-zoom-out">Glemt password</a>
		</li>
	</ul>
</div>

<div id="password-recover-dialog" class="mfp-dialog clearfix mfp-hide">
	<i class="fa fa-key dialog-icon"></i>
	<h3>Glemt dit password?</h3>
	<h5>Har du glemt dit password? Ingen grund til bekymring, for vi kan hjælpe dig! Indtast din e-mailadresse nedenfor. Vi vil så sende dig vejledning om nulstilling din adgangskode.</h5>
	<form method="post" action="/index.php/login/callback/concrete/forgot_password">
		<div class="form-group">
			<input name="uEmail" type="email" placeholder="E-mail adresse" class="form-control">
		</div>
		<button name="resetPassword" class="btn btn-primary btn-block">Nulstil og e-mail kodeord</button>
	</form>
</div>

<div id="sign-up-dialog" class="mfp-dialog clearfix mfp-hide">
	<i class="fa fa-user-plus dialog-icon"></i>
	<h3>Opret dig som bruger på CitiZone</h3>
	<h5>Bliv bruger på CitiZone og få adgang til alle de tekniske dokumenter. Har du allerede en konto, så login <a class="popup-text" href="#login-dialog" data-effect="mfp-zoom-out">her</a></h5>
	<form method="post" action="/index.php/register/do_register" class="form-stacked">
		<div class="form-group">
			<fieldset>
				<div class="form-group">
				<label for="akID[19][value]" class="control-label">Dit navn</label><span class="ccm-required">*</span>
				<input id="akID[19][value]" type="text" name="akID[19][value]" value="" class="span5 form-control ccm-input-text">
				</div>
				<div class="form-group">
				<label for="akID[20][value]" class="control-label">Firmanavn</label>
				<input id="akID[20][value]" type="text" name="akID[20][value]" value="" class="span5 form-control ccm-input-text">
				</div>
				<div class="form-group">
				<label for="uName" class="control-label">Brugernavn</label>                            <input id="uName" type="text" name="uName" value="" class="form-control ccm-input-text">						</div>
				<div class="form-group">
				<label for="uEmail" class="control-label">E-mail adresse</label>                        <input id="uEmail" type="text" name="uEmail" value="" class="form-control ccm-input-text">                    </div>
				<div class="form-group">
				<label for="uPassword" class="control-label">Kodeord</label>					    <input id="uPassword" type="password" name="uPassword" value="" class="form-control ccm-input-password">					</div>
				<div class="form-group">
				<label for="uPasswordConfirm" class="control-label">Bekræft kodeord</label>						<input id="uPasswordConfirm" type="password" name="uPasswordConfirm" value="" class="form-control ccm-input-password">					</div>
				
			</fieldset>
		</div>
		<div class="form-group">
		<label for="captcha" class="control-label">Skriv venligst bogstaverne og tallene præcist som vist på billedet.</label>                        <div><input type="text" name="ccmCaptchaCode" class="ccm-input-captcha" required="required"></div><br><div><img src="/index.php/tools/required/captcha?nocache=1430567079" alt="Captcha kode" onclick="this.src = '/index.php/tools/required/captcha?nocache='+(new Date().getTime())" class="ccm-captcha-image"></div><br><div>Klik på billedet for at se en anden CAPTCHA.</div>					</div>
		<div class="form-group">
			<div class="form-actions">
			<input type="hidden" name="rcID" id="rcID" value="">					
			<input type="submit" class="btn btn-primary ccm-input-submit" id="register" name="register" value="Opret">				</div>
		</div>
	</form>
</div>