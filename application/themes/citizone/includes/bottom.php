<footer class="main" id="main-footer">
	<div class="footer-top-area">
		<div class="container">
			<div class="row row-wrap">
				<div class="col-md-4 r-mb40">
					<a href="/">
						<img src="<?php echo $this->getThemePath(); ?>/images/logo-footer.png" alt="logo" title="logo" class="logo" style="max-width: 220px;">
					</a>
					<ul class="list list-social">
						<li>
							<a class="fa fa-facebook box-icon" href="#"></a>
						</li>
						<li>
							<a class="fa fa-twitter box-icon" href="#"></a>
						</li>
						<li>
							<a class="fa fa-linkedin box-icon" href="#"></a>
						</li>
					</ul>
					<?php 
						$a = new GlobalArea('FooterInfoText');
						$a->display();
					?>
				</div>
				<div class="col-md-4 r-mb40">
					<h4>Tilmeld dig til vores nyhedsbrev</h4>
					<div class="box">
						<form>
							<div class="form-group mb10">
								<label>E-mail</label>
								<input type="text" class="form-control">
							</div>
							<p class="mb10">Tilmeld dig og få tilbud og nyheder direkte i din indbakke!</p>
							<input type="submit" class="btn btn-primary" value="Tilmeld mig">
						</form>
					</div>
				</div>
				<div class="col-md-4">
					<h4>CitiZone på Twitter</h4>
					<div class="twitter-ticker">
						<?php 
							$a = new GlobalArea('Twitter Feed');
							$a->display();
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<p>Copyright © 2015 CitiZone. Design & udvikling af <a href="http://www.geekmedia.dk" target="_blank">Geek Media</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
</footer>
</div>

<!--Dialogs-->
<?php include 'dialogs.php';?>


<?php
	$cp = new Permissions($c);
	$headerBar = is_object($cp) && ($cp->canWrite() || $cp->canAddSubContent() || $cp->canAdminPage() || $cp->canApproveCollection());
	if (!$headerBar){?>
<script src="/concrete/js/jquery.js"></script>
<?php
}
?>
<?php Loader::element('footer_required'); ?>
<script src="<?php echo $this->getThemePath(); ?>/js/tab.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/transition.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/collapse.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/mfp.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/owl.carousel.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/nicescroll.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/tweet.min.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/flexnav.min.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/html5shiv.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/html5shiv-printshiv.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/functions.js"></script>
<!--Start Cookie Script-->
<script type="text/javascript" charset="UTF-8" src="http://chs02.cookie-script.com/s/d57ae735f90e2ea6acf07560197caf00.js"></script>
<!--End Cookie Script-->
</body>
</html>