"use strict";

//Paralax background
$('.bg-parallax').each(function() {
    var $obj = $(this);

    $(window).scroll(function() {
        // var yPos = -($(window).scrollTop() / $obj.data('speed'));
        var animSpeed;
        if ($obj.hasClass('bg-blur')) {
            animSpeed = 10;
        } else {
            animSpeed = 15;
        }
        var yPos = -($(window).scrollTop() / animSpeed);
        var bgpos = '50% ' + yPos + 'px';
        $obj.css('background-position', bgpos);

    });
});

// Responsive navigation
$('#flexnav').flexNav();


//Document ready functions
$(document).ready(function() {
	
	
//Twitter scroll
var element = $('.tf-tweets li'),
    length = element.length, 
    current = 0,
    timeout = 6000;

function changeSlide() {
    element.eq(current++).fadeOut(300, function(){
        if(current === length){
            current = 0;
        }
        
        element.eq(current).fadeIn(300);
    });
    
    setTimeout(changeSlide, timeout);
}

element.slice(1).hide();
setTimeout(changeSlide, timeout);
	
//Nicescroll
 $('html').niceScroll({
        cursorcolor: "#fff",
        cursorborder: "0px solid #fff",
        cursorwidth: "5px",
        cursorborderradius: "0px",
        cursoropacitymin: 0.3,
        cursoropacitymax: 0.7,
        horizrailenabled: false,
        zindex: 9999
    });	
	
//Magnific popup	
  $('.popup-text').magnificPopup({
    removalDelay: 500,
    closeBtnInside: true,
	overflowY: 'hidden',
    callbacks: {
        beforeOpen: function() {
            this.st.mainClass = this.st.el.attr('data-effect');
        }
    },
    midClick: true
});

// Owl Carousel
    var owlCarousel = $('#owl-carousel'),
        owlItems = owlCarousel.attr('data-items'),
        owlCarouselSlider = $('#owl-carousel-slider'),
        owlNav = owlCarouselSlider.attr('data-nav');

    owlCarousel.owlCarousel({
        items: owlItems,
        navigation: true,
        navigationText: ['', '']
    });

    owlCarouselSlider.owlCarousel({
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        navigation: true,
        navigationText: ['', ''],
        transitionStyle: 'goDown',
        autoPlay: 4500,
		stopOnHover: true
    });

//Tabs	
	$('#myTab a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	})
});