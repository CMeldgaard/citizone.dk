<?php include 'includes/header.php';?>
<body>
	
	<div class="container global-wrap">
		<?php include 'includes/menu.php';?>
		<div class="gap"></div>
		<div class="container">
			<div class="col-md-10 col-md-offset-1">
				<?php print $innerContent; ?>
			</div>
		</div>
		<div class="gap"></div>
	<?php include 'includes/bottom.php';?>	