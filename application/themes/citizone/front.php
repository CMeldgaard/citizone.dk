<?php include 'includes/header.php';?>
	<body>
		
		<div class="container global-wrap">
			<?php include 'includes/menu.php';?>
			<div class="top-area hidden-xs">
				<div id="owl-carousel-slider" class="owl-carousel">
					<div> <img src="<?php echo $this->getThemePath(); ?>/images/slides/slide1.jpg" alt="Image Alternative text" title="Bridge"> 
						<div class="vert-center text-white text-center slider-caption">
                            <h2 class="text-uppercase text-white">Velkommen</h2>
                            <div class="sub-text">
								CitiZone tilbyder unikke og gennemarbejde løsninger
							</div><a class="btn btn-lg btn-ghost btn-white" href="/om-citizone">Læs mere</a>
						</div>
					</div>
					<div> <img src="<?php echo $this->getThemePath(); ?>/images/slides/slide2.jpg" alt="Billede fra HL14" title="HL14"> 
						<div class="vert-center text-white text-center slider-caption">
                            <h2 class="text-uppercase text-white">Mød os til hl15</h2>
                            <div class="sub-text">
								Mød os til HL15 i Slagelse d. 26 - 28 august, på stand nr. B054. Vi glæder os til at se dig.
							</div><a class="btn btn-lg btn-ghost btn-white" href="http://hl15.dk/info-til-besoegende/billetter-og-forplejning/" target="_blank">Bestil bilet i dag</a>
						</div>
					</div>
				</div>
			</div>
			<div class="gap"></div>
			<div class="container">
				<div class="col-md-10 col-md-offset-1">
					<div class="text-center">
					<?php 
					$a = new GlobalArea('HTMLBox');
					$a->display();
					?>
					</div>
				</div>
			</div>
			<div class="gap"></div>
			<div class="bg-holder">
				<div class="bg-mask"></div>
				<div class="bg-parallax" style="background-image: url(<?php echo $this->getThemePath(); ?>/images/paralaxBg.jpg);"></div>
				<div class="container bg-holder-content">
					<div class="gap gap-big text-center disableRow">
						<h1 class="mb20 text-white">CitiZone anbefaler</h1>
							<?php 
								$a = new Area('Recomended Products');
								$a->display($c);
								?>
					</div>
				</div>
			</div>
			<div class="gap"></div>
<?php include 'includes/bottom.php';?>