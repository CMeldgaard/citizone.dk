<?php include 'includes/header.php';?>
<body>
	<div class="container global-wrap">
		<?php include 'includes/menu.php';?>
		<div class="gap"></div>
		<div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php 
						$nh = Loader::helper('navigation');
						$_c = $nh->getTrailToCollection($c);
						$page = Page::getByID($_c[0]->getCollectionParentID());
						print '<h2>';
						print $page->getCollectionName();
						print '</h2>';
						
						$a = new GlobalArea('Sidebar productgroup');
						$a->display();
					?>
				</div>
                <div class="col-md-9">
					<div class="row">
                        <div class="col-sm-6 col-md-7">
                           <?php
									$a = new Area('Product page slider holder');
									$a->display($c);
								?>     
						</div>
                        <div class="col-sm-6 col-md-5">
                            <div class="product-info box">
								<?php
									$a = new GlobalArea('Product page titel');
									$a->display();
									
									$c = Page::getCurrentPage();
									$strDesc = $c->getCollectionDescription();
									print $strDesc;
								?>     
								<div class="small-gap"></div>
								<h3>Del produktet</h3>
								<?php
									$a = new GlobalArea('Product page social share');
									$a->display();
								?> 
							</div>
							
						</div>
						<div class="gap">
							
						</div>
						<div class="col-md-12">
						<div class="tabbable">
							<ul class="nav nav-tabs" id="myTab">
								<li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-pencil"></i> Beskrivelse</a>
								</li>
								<li><a href="#tab-2" data-toggle="tab"><i class="fa fa-info"></i> Specifikationer</a>
								</li>
								<li><a href="#tab-3" data-toggle="tab"><i class="fa fa-download"></i> Downloads</a>
								</li>
								<li><a href="#tab-4" data-toggle="tab"><i class="fa fa-comments"></i> Anmeldelser</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab-1">
									<?php
										$a = new Area('Product page long description');
										$a->display($c);
									?>
								</div>
								<div class="tab-pane fade specs" id="tab-2">
									<?php
										$a = new Area('Product page specifikationer');
										$a->display($c);
									?>
								</div>
								<div class="tab-pane fade" id="tab-3">
									<h4>Brochurer & artikler</h4>
									<div class="col-md-6">
										<?php
										$a = new Area('Product page file area 1');
										$a->display($c);
									?>
									</div>
									<div class="col-md-6">
									<?php
										$a = new Area('Product page file area 2');
										$a->display($c);
									?>
									</div>
									<div class="small-gap"></div>
									<?php
								global $u;
								if ($u -> isLoggedIn ()) {?>

									<h4>Installationsvejledninger</h4>
									<div class="col-md-6">
									<?php
										$a = new Area('Product page file area 3');
										$a->display($c);
									?>
									</div>
									<div class="col-md-6">
									<?php
										$a = new Area('Product page file area 4');
										$a->display($c);
									?>
									</div>
									<div class="small-gap"></div>
									<h4>Tekniske detaljer</h4>
									<div class="col-md-6">
									<?php
										$a = new Area('Product page file area 5');
										$a->display($c);
									?>
									</div>
									<div class="col-md-6">
									<?php
										$a = new Area('Product page file area 6');
										$a->display($c);
									?>
									</div>
									<?php
									}else{
									?>
									Du skal være logget ind for at få adgang til de tekniske filer.
									<?php
									}
									?>
									<div class="small-gap"></div>
								</div>
								<div class="tab-pane fade" id="tab-4">
								<?php

										$a = new Area('Product Pages Comments');
										$a->display($c);

								?>
								</div>
							</div>
						</div>
						</div>
					</div>
					</div></div>
				</div>
				<div class="gap"></div>
			<?php include 'includes/bottom.php';?>					