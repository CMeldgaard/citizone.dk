<?php include 'includes/header.php';?>
<body>
	<div class="container global-wrap">
		<?php include 'includes/menu.php';?>
		<div class="gap"></div>
		<div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php 
					$c->getCollectionName(); //displays the name of the current page
					$page = Page::getByID($c->getCollectionParentID());
					print '<h2>';
					print $page->getCollectionName(); // gets the parent name
					print '</h2>';
					
							$a = new GlobalArea('Sidebar productgroup');
							$a->display();
						?>
				</div>
                <div class="col-md-9">
					<?php 
					$c = Page::getCurrentPage();
					$strTitel = $c->getCollectionName();
					print "<h1>".$strTitel."</h1>";?>
					<div class="row">
					<div class="col-md-8">
					<?php $a = new Area('GroupDescription');
					$a->display($c); ?>
					</div>
					<div class="col-md-4">
					<?php $a = new Area('GroupSlideshow');
					$a->display($c); ?>
					</div>
					</div>
					<div class="small-gap">
					</div>
					<div class="r-mlr15">
						<?php 
							$a = new GlobalArea('Productgrid');
							$a->display();
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="gap"></div>
	<?php include 'includes/bottom.php';?>	