<?php include 'includes/header.php';?>
<body>
	
	<div class="container global-wrap">
		<?php include 'includes/menu.php';?>
		<div class="gap"></div>
		<div class="container">
		<div class="row">
			<div class="col-md-3">
                     <?php 
					// Set your level here
					// 0 = the first level after home page level (home => CHILD)
					// 1 = the second level (home => child => CHILD)
					// etc.
					$level = 0;
					 
					$current = $p = Page::getCurrentPage();
					$tree = array();
					while ($p->getCollectionParentID() >= HOME_CID) {
					   array_push($tree, $p);
					   $p  = Page::getByID($p->getCollectionParentID());
					}
					$tree = array_reverse($tree);
					if (isset($tree[$level])) {
					   $parent = $tree[$level];
					   echo '<h2>' .  $parent->getCollectionName() .  '</h2>';
					}
					
					$a = new GlobalArea('Sidebar productgroup');
					$a->display();
						?>
				</div>
			<div class="col-md-9">
					<?php $a = new Area('Headline');
					$a->display($c); ?>
				<div class="gap"></div>
				<?php $a = new Area('Content');
					$a->display($c); ?>
			</div>
		</div>
		</div>
		<div class="gap"></div>
	<?php include 'includes/bottom.php';?>	