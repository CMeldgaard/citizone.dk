<?php include 'includes/header.php';?>
<body>
	
	<div class="container global-wrap">
		<?php include 'includes/menu.php';?>
		<div class="gap"></div>
		<div class="container">
			<div class="col-md-10 col-md-offset-1">
				<div class="text-center">
					<?php $a = new Area('Headline');
					$a->display($c); ?>
				</div>
			</div>
		</div>
		<div class="gap"></div>
		<div class="container">
            <div class="row row-wrap">
                <div class="col-md-6 r-mb40">
                    <?php $a = new Area('Google Maps');
					$a->display($c); ?>
                </div>
                <div class="col-md-3 r-mb40">
                   <?php $a = new Area('Kontakt formular');
					$a->display($c); ?>
                </div>
                <div class="col-md-3 r-mb40">
				<?php $a = new Area('Kontakt info');
					$a->display($c); ?>
                </div>
            </div>
            <div class="gap gap-small"></div>
        </div>
		
		<div class="gap"></div>
	<?php include 'includes/bottom.php';?>	