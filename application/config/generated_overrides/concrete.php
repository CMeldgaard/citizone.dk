<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2015-05-20T22:45:56+02:00
 *
 * @item      misc.do_page_reindex_check
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return array(
    'locale' => 'da_DK',
    'site' => 'CitiZone',
    'version_installed' => '5.7.3.1',
    'misc' => array(
        'access_entity_updated' => 1429257206,
        'latest_version' => '5.7.3.1',
        'seen_introduction' => true,
        'do_page_reindex_check' => false
    ),
    'seo' => array(
        'url_rewriting' => 1,
        'tracking' => array(
            'code' => '<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-45624510-4\', \'auto\');
  ga(\'send\', \'pageview\');

</script>',
            'code_position' => 'bottom'
        )
    ),
    'debug' => array(
        'detail' => 'debug',
        'display_errors' => true
    ),
    'cache' => array(
        'blocks' => true,
        'assets' => true,
        'theme_css' => true,
        'overrides' => true,
        'pages' => 'blocks',
        'full_page_lifetime' => 'forever',
        'full_page_lifetime_value' => null
    ),
    'theme' => array(
        'compress_preprocessor_output' => true
    ),
    'user' => array(
        'registration' => array(
            'email_registration' => true,
            'type' => 'enabled',
            'captcha' => true,
            'enabled' => true,
            'validate_email' => false,
            'approval' => false,
            'notification' => null,
            'notification_email' => ''
        )
    ),
    'upload' => array(
        'extensions' => '*.flv;*.jpg;*.gif;*.jpeg;*.ico;*.docx;*.xla;*.png;*.psd;*.swf;*.doc;*.txt;*.xls;*.xlsx;*.csv;*.pdf;*.tiff;*.rtf;*.m4a;*.mov;*.wmv;*.mpeg;*.mpg;*.wav;*.3gp;*.avi;*.m4v;*.mp4;*.mp3;*.qt;*.ppt;*.pptx;*.kml;*.xml;*.svg;*.webm;*.ogg;*.ogv;*.zip;*.dwg'
    ),
    'maintenance' => array(
        'version_job_page_num' => 1
    )
);
