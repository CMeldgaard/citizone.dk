<?php

return array(
    'default-connection' => 'concrete',
    'connections' => array(
        'concrete' => array(
            'driver' => 'c5_pdo_mysql',
            'server' => 'localhost:3306',
            'database' => 'citizone_dk',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8'
        )
    )
);
