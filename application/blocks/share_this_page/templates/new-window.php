<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="ccm-block-share-this-page">
    <ul class="list-inline">
    <?php foreach($selected as $service) { 
		
		$serviceType = $service->getHandle();

	if ($serviceType == "email"){?>
		<li><a href="<?php echo $service->getServiceLink()?>"><?php echo $service->getServiceIconHTML()?></a></li>
		<?php
		}else{
		?>
        <li><a href="<?php echo $service->getServiceLink()?>" onclick="window.open('<?php echo $service->getServiceLink()?>', 'newwindow', 'width=575, height=250'); return false;"><?php echo $service->getServiceIconHTML()?></a></li>
    <?php
		} 
	} 
	?>
    </ul>
</div>