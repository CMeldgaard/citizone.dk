<?php 
defined('C5_EXECUTE') or die("Access Denied.");

$page = Page::getCurrentPage();


if($page instanceof Page) {
	$cID = $page->getCollectionID();
}

?>	
	
<?php    if (!empty($files)) { ?>	

 	<?php 
	foreach($files as $f) {
		  
		$fv = $f->getApprovedVersion();
		
		// although the 'title' for a file is used for display,
		// the filename is retreived here so we can always get a file extension
		$filename = $fv->getFileName();
		$ext =  pathinfo($filename, PATHINFO_EXTENSION);
		$url = $fv->getURL();

		 	// if you want to add more information about a file (e.g. description, download count)
		 	// look up in the API the functions for a File object and FileVersion object ($f and $fv in above code)
		?>
	 
		
		<div class="col-md-3 col-sm-4 col-xs-12 mb20 text-center photoRef">
		<img src="<?php echo $url; ?>" />
		</div>

<?php    }	?>


<?php    }	?>

<?php  if ($pagination): ?>
    <?php  echo $pagination;?>
<?php  endif; ?>


<?php    if (empty($files) && $noFilesMessage) { ?>
<p><?php    echo $noFilesMessage; ?></p>
<?php    } ?>
