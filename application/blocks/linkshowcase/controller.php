<?php

namespace Application\Block\Linkshowcase;

use Core;
use Loader;
use Page;
use Concrete\Core\Block\BlockController;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends BlockController
{
	protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 550;
    protected $btTable = "btLinkShowcase";
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btWrapperClass = 'ccm-ui';
    protected $btExportPageColumns = array('internalLinkCID');

    public function getBlockTypeName()
    {
        return t('ekstern link');
    }

    public function validate($data)
    {
        //$e = Core::make('error');
        //if (!$data['pageTitel']) {
        //    $e->add(t('Du skal skrive en knap tekst'));
        //}

        //return $e;
    }

    public function getBlockTypeDescription()
    {
        return t('Tilføje ekstern link med automatisk billede');
    }
	
	function getExternalLink() {return $this->externalLink;}
    function getInternalLinkCID() {return $this->internalLinkCID;}
    function getLinkURL() {
        if (!empty($this->externalLink)) {
            return $this->externalLink;
        } else if (!empty($this->internalLinkCID)) {
            $linkToC = Page::getByID($this->internalLinkCID);
            return (empty($linkToC) || $linkToC->error) ? '' : Loader::helper('navigation')->getLinkToCollection($linkToC);
        } else {
            return '';
        }
    }
	
	public function view() {
	
	$this->set(
		  'blockIdentifier',
		  $this->getBlockObject()->getProxyBlock()
			? $this->getBlockObject()->getProxyBlock()->getInstance()->getIdentifier()
			: $this->getIdentifier()
		);

	$this->set('linkURL',$this->getLinkURL());
		if (!empty($this->externalLink)) {
			$this->set('isExt','1');
		}
    }
	
    public function save($args) {

        switch (intval($args['linkType'])) {
            case 1:
                $args['externalLink'] = '';
                break;
            case 2:
                $args['internalLinkCID'] = 0;
                break;
            default:
                $args['externalLink'] = '';
                $args['internalLinkCID'] = 0;
                break;
        }
		
        unset($args['linkType']); //this doesn't get saved to the database (it's only for UI usage)
        parent::save($args);
		
		
    }
}
