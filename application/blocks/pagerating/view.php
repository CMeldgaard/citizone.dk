<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php   
$form = Loader::helper('form');
$token = Loader::helper('validation/token');
?>

<div class="clearfix"></div>
<div id="c5dk_rating_b<?php  echo $bID; ?>">
	<?php  echo $form->hidden("votesID", $votesID); ?>
	<?php  echo $form->hidden("rating", $rating); ?>
	<span id="c5dk_rating_frame">
		<?php  echo str_replace("%COUNT%", '<span class="c5dk_rating_count">' . $count . '</span>', $textLeft); ?>
		<i id="rating1" class="fa <?php  echo ($rating >=1)? $fullChar . ' c5dk_fullChar' : $emptyChar . ' c5dk_emptyChar'; ?>"></i><i id="rating2" class="fa <?php  echo ($rating >=2)? $fullChar . ' c5dk_fullChar' : $emptyChar . ' c5dk_emptyChar'; ?>"></i><i id="rating3" class="fa <?php  echo ($rating >=3)? $fullChar . ' c5dk_fullChar' : $emptyChar . ' c5dk_emptyChar'; ?>"></i><i id="rating4" class="fa <?php  echo ($rating >=4)? $fullChar . ' c5dk_fullChar' : $emptyChar . ' c5dk_emptyChar'; ?>"></i><i id="rating5" class="fa <?php  echo ($rating >=5)? $fullChar . ' c5dk_fullChar' : $emptyChar . ' c5dk_emptyChar'; ?>"></i>
		<?php  echo str_replace("%COUNT%", '<span class="c5dk_rating_count">' . $count . '</span>', $textRight); ?>
		<span class="c5dk_rating_saveSpinner"><i class="fa fa-spinner fa-spin"></i></span>
	</span>
</div>
<div class="clearfix"></div>

<style>
	#c5dk_rating_b<?php  echo $bID; ?> {
		clear: both;
		cursor: pointer;
		<?php  if($align == 'center'){ ?>text-align: center;<?php  } ?>
	}
	#c5dk_rating_b<?php  echo $bID; ?> #c5dk_rating_frame {
		<?php  if($align == 'left'){ ?>float: left;<?php  } ?>
		<?php  if($align == 'right'){ ?>float: right;<?php  } ?>
	}
	#c5dk_rating_b<?php  echo $bID; ?> .c5dk_emptyChar {
		font-size: <?php  echo $emptySize; ?>px;
		color: <?php  echo $emptyColor; ?>;
		width: 20px;
	}
	#c5dk_rating_b<?php  echo $bID; ?> .c5dk_fullChar {
		font-size: <?php  echo $fullSize; ?>px;
		color: <?php  echo $fullColor; ?>;
		width: 20px;
	}
	#c5dk_rating_b<?php  echo $bID; ?> .c5dk_rating_saveSpinner{
		display: none;
	}
</style>

<script type="text/javascript">
	if(!c5dk){ var c5dk = {} };
	if(!c5dk.rating){ c5dk.rating = {} };

	if(!c5dk.rating.data){ c5dk.rating.data = {} };
	c5dk.rating.data.b<?php  echo $bID; ?> = {
		rating:<?php  echo $rating; ?>,
		votes:<?php  echo $count; ?>,
		emptyChar:"<?php  echo $emptyChar; ?>",
		fullChar:"<?php  echo $fullChar; ?>",
		url:"<?php  echo $this->action('vote'); ?>",
		saving:false
	}
	c5dk.rating.token = "<?php  echo $token->generate('c5dk_rating_vote'); ?>";

	$(document).ready(function() {
		if (navigator.cookieEnabled) {
			$("#c5dk_rating_b<?php  echo $bID; ?> i").mouseover(function(event) {
				c5dk.rating.mouseover("b<?php  echo $bID; ?>", event);
			});
			$("#c5dk_rating_b<?php  echo $bID; ?>").mouseout(function(event) {
				c5dk.rating.mouseout("b<?php  echo $bID; ?>");
			});
			$("#c5dk_rating_b<?php  echo $bID; ?> i").click(function(event) {
				c5dk.rating.vote(<?php  echo $bID; ?>, <?php  echo Page::getCurrentPage()->getCollectionID(); ?>, <?php  echo $votesID; ?>, event, "<?php  echo $toolsURL; ?>");
			});
		} else {
			$("#c5dk_rating_b<?php  echo $bID; ?>").attr('title', '<?php  echo t("Cookies needs to be enabled to vote."); ?>');
		}
	});
</script>