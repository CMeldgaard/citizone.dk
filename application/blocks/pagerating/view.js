if(!c5dk){ var c5dk = {} };
if(!c5dk.rating){ c5dk.rating = {} };

c5dk.rating.mouseover = function(bID, event) {
	c5dk.rating.update(bID, event.target.id.substr(6));
}

c5dk.rating.mouseout = function(bID) {
	c5dk.rating.update(bID, c5dk.rating.data[bID].rating);
}

c5dk.rating.update = function(bID, rating){
	if (c5dk.rating.data[bID].saving == false){
		if(rating == 0){
			$("#c5dk_rating_" + bID + " i")
				.removeClass(c5dk.rating.data[bID].fullChar)
				.addClass(c5dk.rating.data[bID].emptyChar)
				.removeClass('c5dk_fullChar')
				.addClass('c5dk_emptyChar');
		} else {
			$("#c5dk_rating_" + bID + " i:lt(" + rating + ")")
				.removeClass(c5dk.rating.data[bID].emptyChar)
				.addClass(c5dk.rating.data[bID].fullChar)
				.removeClass('c5dk_emptyChar')
				.addClass('c5dk_fullChar');
			$("#c5dk_rating_" + bID + " i:gt(" + (rating-1) + ")")
				.removeClass(c5dk.rating.data[bID].fullChar)
				.addClass(c5dk.rating.data[bID].emptyChar)
				.removeClass('c5dk_fullChar')
				.addClass('c5dk_emptyChar');
		}
	}
}

c5dk.rating.vote = function(bID, cID, votesID, event, url){
	// Set data values
	var rating = event.target.id.substr(6);
	c5dk.rating.data["b" + bID].saving = true;
	c5dk.rating.data["b" + bID].rating = rating;

	// Show save spinner
	$("#c5dk_rating_b" + bID + " .c5dk_rating_saveSpinner").show();

	// Update rating
	c5dk.rating.update("b" + bID, rating);
	id = this.readCookie("c5dk_rating_" + cID + "_" + bID + "_" + votesID);
	if (!id) {
		// Make id
		id = new Date().getTime();

		// Update the %COUNT% number if exsist
		$("#c5dk_rating_b" + bID + " .c5dk_rating_count").text(++c5dk.rating.data["b" + bID].votes);
	}
	this.createCookie("c5dk_rating_" + cID + "_" + bID + "_" + votesID, id, 365);
	$.ajax({
		type: "POST",
		dataType: "json",
		url: url,
		data: {"cID":cID, "votesID":votesID, "id":id, "rating": rating, "ccm_token": c5dk.rating.token},
		success: function(json, status){
			c5dk.rating.data["b" + bID].saving = false;

			// Hide save spinner
			$("#c5dk_rating_b" + bID + " .c5dk_rating_saveSpinner").hide();
		}
	});
}

c5dk.rating.createCookie = function(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	} else {
		var expires = "";
	}
	document.cookie = name+"="+value+expires+"; path=/";
}

c5dk.rating.readCookie = function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}