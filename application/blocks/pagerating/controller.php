<?php
namespace Application\Block\Pagerating;

use Core;
use Loader;
use Page;
use Concrete\Core\Block\BlockController;

class Controller extends BlockController{

	protected $btTable = 'btC5dkRating';
	protected $btInterfaceWidth = 400;
	protected $btInterfaceHeight = 400; 
	protected $btCacheBlockRecord = false;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	public $rating;
	public $count;

	public function getBlockTypeName() { return t("Side rating"); }
	public function getBlockTypeDescription() { return t("Rating system for your website."); }

	public function on_page_view(){
		$this->set('rating',		$this->getRating(Page::getCurrentPage()->getCollectionID()));
		$this->set('count',			($this->count)? $this->count : "0");
		$this->set("toolsURL",	Loader::helper('concrete/urls')->getToolsURL('vote', 'c5dk_rating'));
		$this->addHeaderItems();
	}

	public function add(){
		// Generate a votesID
		$this->set('votesID', time());

		// Default values
		$this->set('emptyChar',		'fa-star-o');
		$this->set('emptySize',		'18');
		$this->set('emptyColor',	'#000000');
		$this->set('fullChar',		'fa-star');
		$this->set('fullSize',		'18');
		$this->set('fullColor',		'#e3cf7a');
		$this->set('align',				'left');
		$this->set('textRight',		t('(Votes: %COUNT%)'));

		$this->addHeaderItems();
	}

	public function edit(){
		$this->addHeaderItems();
	}

	public function save($args){
		// Turn off page ratings in other C5DK Rating blocks on the current page
		if ($args["pageRating"]) {
			$db = Loader::db();
			foreach (Page::getCurrentPage()->getBlocks() as $block) {
				if ($block->getBlockTypeHandle() == "c5dk_rating") {
					$bi = $block->getInstance();
					if ($bi->votesID <> $args["votesID"]) {
						$rs = $db->Execute('UPDATE btC5dkRating SET pageRating=0 WHERE bID=?', array($bi->bID));
					}
				}
			}
		}
		parent::save($args);
	}

	public function getRating($cID){
		$db = Loader::db();
		$rowData = array(
			$this->votesID,
			$cID
		);
		$rs = $db->Execute('SELECT * FROM btC5dkRatingVotes WHERE votesID=? AND cID=?', $rowData);
		$countRS = $db->Execute('SELECT COUNT(*) as voteCount FROM btC5dkRatingVotes WHERE votesID=? AND cID=?', $rowData);
		$voteCount = $countRS->fields["voteCount"];
		
		if ($voteCount == 0) {
			// No votes
			$this->count = 0;
			return 0;
		}	else {
			$rsVote = $db->Execute('SELECT avg(rating) as average_vote FROM btC5dkRatingVotes WHERE votesID=? AND cID=?', $rowData);
			$averageVote = $rsVote->fields["average_vote"];
			$this->count = $voteCount;
			return round($averageVote);
		}
	}

	public function setRating($cID){
		$this->rating = $this->getRating($cID);
	}

	public function addHeaderItems(){
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'));
	}

}