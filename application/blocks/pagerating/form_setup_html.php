<?php  //defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php 
$form = Loader::helper('form');

$faOptions = array('fa-adjust' => 'fa-adjust', 'fa-adn' => 'fa-adn', 'fa-align-center' => 'fa-align-center', 'fa-align-justify' => 'fa-align-justify', 'fa-align-left' => 'fa-align-left', 'fa-align-right' => 'fa-align-right', 'fa-ambulance' => 'fa-ambulance', 'fa-anchor' => 'fa-anchor', 'fa-android' => 'fa-android', 'fa-angle-double-down' => 'fa-angle-double-down', 'fa-angle-double-left' => 'fa-angle-double-left', 'fa-angle-double-right' => 'fa-angle-double-right', 'fa-angle-double-up' => 'fa-angle-double-up', 'fa-angle-down' => 'fa-angle-down', 'fa-angle-left' => 'fa-angle-left', 'fa-angle-right' => 'fa-angle-right', 'fa-angle-up' => 'fa-angle-up', 'fa-apple' => 'fa-apple', 'fa-archive' => 'fa-archive', 'fa-arrow-circle-down' => 'fa-arrow-circle-down', 'fa-arrow-circle-left' => 'fa-arrow-circle-left', 'fa-arrow-circle-o-down' => 'fa-arrow-circle-o-down', 'fa-arrow-circle-o-left' => 'fa-arrow-circle-o-left', 'fa-arrow-circle-o-right' => 'fa-arrow-circle-o-right', 'fa-arrow-circle-o-up' => 'fa-arrow-circle-o-up', 'fa-arrow-circle-right' => 'fa-arrow-circle-right', 'fa-arrow-circle-up' => 'fa-arrow-circle-up', 'fa-arrow-down' => 'fa-arrow-down', 'fa-arrow-left' => 'fa-arrow-left', 'fa-arrow-right' => 'fa-arrow-right', 'fa-arrow-up' => 'fa-arrow-up', 'fa-arrows' => 'fa-arrows', 'fa-arrows-alt' => 'fa-arrows-alt', 'fa-arrows-h' => 'fa-arrows-h', 'fa-arrows-v' => 'fa-arrows-v', 'fa-asterisk' => 'fa-asterisk', 'fa-backward' => 'fa-backward', 'fa-ban' => 'fa-ban', 'fa-bar-chart-o' => 'fa-bar-chart-o', 'fa-barcode' => 'fa-barcode', 'fa-bars' => 'fa-bars', 'fa-beer' => 'fa-beer', 'fa-bell' => 'fa-bell', 'fa-bell-o' => 'fa-bell-o', 'fa-bitbucket' => 'fa-bitbucket', 'fa-bitbucket-square' => 'fa-bitbucket-square', 'fa-bold' => 'fa-bold', 'fa-bolt' => 'fa-bolt', 'fa-book' => 'fa-book', 'fa-bookmark' => 'fa-bookmark', 'fa-bookmark-o' => 'fa-bookmark-o', 'fa-briefcase' => 'fa-briefcase', 'fa-btc' => 'fa-btc', 'fa-bug' => 'fa-bug', 'fa-building-o' => 'fa-building-o', 'fa-bullhorn' => 'fa-bullhorn', 'fa-bullseye' => 'fa-bullseye', 'fa-calendar' => 'fa-calendar', 'fa-calendar-o' => 'fa-calendar-o', 'fa-camera' => 'fa-camera', 'fa-camera-retro' => 'fa-camera-retro', 'fa-caret-down' => 'fa-caret-down', 'fa-caret-left' => 'fa-caret-left', 'fa-caret-right' => 'fa-caret-right', 'fa-caret-square-o-down' => 'fa-caret-square-o-down', 'fa-caret-square-o-left' => 'fa-caret-square-o-left', 'fa-caret-square-o-right' => 'fa-caret-square-o-right', 'fa-caret-square-o-up' => 'fa-caret-square-o-up', 'fa-caret-up' => 'fa-caret-up', 'fa-certificate' => 'fa-certificate', 'fa-chain-broken' => 'fa-chain-broken', 'fa-check' => 'fa-check', 'fa-check-circle' => 'fa-check-circle', 'fa-check-circle-o' => 'fa-check-circle-o', 'fa-check-square' => 'fa-check-square', 'fa-check-square-o' => 'fa-check-square-o', 'fa-chevron-circle-down' => 'fa-chevron-circle-down', 'fa-chevron-circle-left' => 'fa-chevron-circle-left', 'fa-chevron-circle-right' => 'fa-chevron-circle-right', 'fa-chevron-circle-up' => 'fa-chevron-circle-up', 'fa-chevron-down' => 'fa-chevron-down', 'fa-chevron-left' => 'fa-chevron-left', 'fa-chevron-right' => 'fa-chevron-right', 'fa-chevron-up' => 'fa-chevron-up', 'fa-circle' => 'fa-circle', 'fa-circle-o' => 'fa-circle-o', 'fa-clipboard' => 'fa-clipboard', 'fa-clock-o' => 'fa-clock-o', 'fa-cloud' => 'fa-cloud', 'fa-cloud-download' => 'fa-cloud-download', 'fa-cloud-upload' => 'fa-cloud-upload', 'fa-code' => 'fa-code', 'fa-code-fork' => 'fa-code-fork', 'fa-coffee' => 'fa-coffee', 'fa-cog' => 'fa-cog', 'fa-cogs' => 'fa-cogs', 'fa-columns' => 'fa-columns', 'fa-comment' => 'fa-comment', 'fa-comment-o' => 'fa-comment-o', 'fa-comments' => 'fa-comments', 'fa-comments-o' => 'fa-comments-o', 'fa-compass' => 'fa-compass', 'fa-compress' => 'fa-compress', 'fa-credit-card' => 'fa-credit-card', 'fa-crop' => 'fa-crop', 'fa-crosshairs' => 'fa-crosshairs', 'fa-css3' => 'fa-css3', 'fa-cutlery' => 'fa-cutlery', 'fa-desktop' => 'fa-desktop', 'fa-dot-circle-o' => 'fa-dot-circle-o', 'fa-download' => 'fa-download', 'fa-dribbble' => 'fa-dribbble', 'fa-dropbox' => 'fa-dropbox', 'fa-eject' => 'fa-eject', 'fa-ellipsis-h' => 'fa-ellipsis-h', 'fa-ellipsis-v' => 'fa-ellipsis-v', 'fa-envelope' => 'fa-envelope', 'fa-envelope-o' => 'fa-envelope-o', 'fa-eraser' => 'fa-eraser', 'fa-eur' => 'fa-eur', 'fa-exchange' => 'fa-exchange', 'fa-exclamation' => 'fa-exclamation', 'fa-exclamation-circle' => 'fa-exclamation-circle', 'fa-exclamation-triangle' => 'fa-exclamation-triangle', 'fa-expand' => 'fa-expand', 'fa-external-link' => 'fa-external-link', 'fa-external-link-square' => 'fa-external-link-square', 'fa-eye' => 'fa-eye', 'fa-eye-slash' => 'fa-eye-slash', 'fa-facebook' => 'fa-facebook', 'fa-facebook-square' => 'fa-facebook-square', 'fa-fast-backward' => 'fa-fast-backward', 'fa-fast-forward' => 'fa-fast-forward', 'fa-female' => 'fa-female', 'fa-fighter-jet' => 'fa-fighter-jet', 'fa-file' => 'fa-file', 'fa-file-o' => 'fa-file-o', 'fa-file-text' => 'fa-file-text', 'fa-file-text-o' => 'fa-file-text-o', 'fa-files-o' => 'fa-files-o', 'fa-film' => 'fa-film', 'fa-filter' => 'fa-filter', 'fa-fire' => 'fa-fire', 'fa-fire-extinguisher' => 'fa-fire-extinguisher', 'fa-flag' => 'fa-flag', 'fa-flag-checkered' => 'fa-flag-checkered', 'fa-flag-o' => 'fa-flag-o', 'fa-flask' => 'fa-flask', 'fa-flickr' => 'fa-flickr', 'fa-floppy-o' => 'fa-floppy-o', 'fa-folder' => 'fa-folder', 'fa-folder-o' => 'fa-folder-o', 'fa-folder-open' => 'fa-folder-open', 'fa-folder-open-o' => 'fa-folder-open-o', 'fa-font' => 'fa-font', 'fa-forward' => 'fa-forward', 'fa-foursquare' => 'fa-foursquare', 'fa-frown-o' => 'fa-frown-o', 'fa-gamepad' => 'fa-gamepad', 'fa-gavel' => 'fa-gavel', 'fa-gbp' => 'fa-gbp', 'fa-gift' => 'fa-gift', 'fa-github' => 'fa-github', 'fa-github-alt' => 'fa-github-alt', 'fa-github-square' => 'fa-github-square', 'fa-gittip' => 'fa-gittip', 'fa-glass' => 'fa-glass', 'fa-globe' => 'fa-globe', 'fa-google-plus' => 'fa-google-plus', 'fa-google-plus-square' => 'fa-google-plus-square', 'fa-h-square' => 'fa-h-square', 'fa-hand-o-down' => 'fa-hand-o-down', 'fa-hand-o-left' => 'fa-hand-o-left', 'fa-hand-o-right' => 'fa-hand-o-right', 'fa-hand-o-up' => 'fa-hand-o-up', 'fa-hdd-o' => 'fa-hdd-o', 'fa-headphones' => 'fa-headphones', 'fa-heart' => 'fa-heart', 'fa-heart-o' => 'fa-heart-o', 'fa-home' => 'fa-home', 'fa-hospital-o' => 'fa-hospital-o', 'fa-html5' => 'fa-html5', 'fa-inbox' => 'fa-inbox', 'fa-indent' => 'fa-indent', 'fa-info' => 'fa-info', 'fa-info-circle' => 'fa-info-circle', 'fa-inr' => 'fa-inr', 'fa-instagram' => 'fa-instagram', 'fa-italic' => 'fa-italic', 'fa-jpy' => 'fa-jpy', 'fa-key' => 'fa-key', 'fa-keyboard-o' => 'fa-keyboard-o', 'fa-krw' => 'fa-krw', 'fa-laptop' => 'fa-laptop', 'fa-leaf' => 'fa-leaf', 'fa-lemon-o' => 'fa-lemon-o', 'fa-level-down' => 'fa-level-down', 'fa-level-up' => 'fa-level-up', 'fa-lightbulb-o' => 'fa-lightbulb-o', 'fa-link' => 'fa-link', 'fa-linkedin' => 'fa-linkedin', 'fa-linkedin-square' => 'fa-linkedin-square', 'fa-linux' => 'fa-linux', 'fa-list' => 'fa-list', 'fa-list-alt' => 'fa-list-alt', 'fa-list-ol' => 'fa-list-ol', 'fa-list-ul' => 'fa-list-ul', 'fa-location-arrow' => 'fa-location-arrow', 'fa-lock' => 'fa-lock', 'fa-long-arrow-down' => 'fa-long-arrow-down', 'fa-long-arrow-left' => 'fa-long-arrow-left', 'fa-long-arrow-right' => 'fa-long-arrow-right', 'fa-long-arrow-up' => 'fa-long-arrow-up', 'fa-magic' => 'fa-magic', 'fa-magnet' => 'fa-magnet', 'fa-mail-reply-all' => 'fa-mail-reply-all', 'fa-male' => 'fa-male', 'fa-map-marker' => 'fa-map-marker', 'fa-maxcdn' => 'fa-maxcdn', 'fa-medkit' => 'fa-medkit', 'fa-meh-o' => 'fa-meh-o', 'fa-microphone' => 'fa-microphone', 'fa-microphone-slash' => 'fa-microphone-slash', 'fa-minus' => 'fa-minus', 'fa-minus-circle' => 'fa-minus-circle', 'fa-minus-square' => 'fa-minus-square', 'fa-minus-square-o' => 'fa-minus-square-o', 'fa-mobile' => 'fa-mobile', 'fa-money' => 'fa-money', 'fa-moon-o' => 'fa-moon-o', 'fa-music' => 'fa-music', 'fa-outdent' => 'fa-outdent', 'fa-pagelines' => 'fa-pagelines', 'fa-paperclip' => 'fa-paperclip', 'fa-pause' => 'fa-pause', 'fa-pencil' => 'fa-pencil', 'fa-pencil-square' => 'fa-pencil-square', 'fa-pencil-square-o' => 'fa-pencil-square-o', 'fa-phone' => 'fa-phone', 'fa-phone-square' => 'fa-phone-square', 'fa-picture-o' => 'fa-picture-o', 'fa-pinterest' => 'fa-pinterest', 'fa-pinterest-square' => 'fa-pinterest-square', 'fa-plane' => 'fa-plane', 'fa-play' => 'fa-play', 'fa-play-circle' => 'fa-play-circle', 'fa-play-circle-o' => 'fa-play-circle-o', 'fa-plus' => 'fa-plus', 'fa-plus-circle' => 'fa-plus-circle', 'fa-plus-square' => 'fa-plus-square', 'fa-plus-square-o' => 'fa-plus-square-o', 'fa-power-off' => 'fa-power-off', 'fa-print' => 'fa-print', 'fa-puzzle-piece' => 'fa-puzzle-piece', 'fa-qrcode' => 'fa-qrcode', 'fa-question' => 'fa-question', 'fa-question-circle' => 'fa-question-circle', 'fa-quote-left' => 'fa-quote-left', 'fa-quote-right' => 'fa-quote-right', 'fa-random' => 'fa-random', 'fa-refresh' => 'fa-refresh', 'fa-renren' => 'fa-renren', 'fa-repeat' => 'fa-repeat', 'fa-reply' => 'fa-reply', 'fa-reply-all' => 'fa-reply-all', 'fa-retweet' => 'fa-retweet', 'fa-road' => 'fa-road', 'fa-rocket' => 'fa-rocket', 'fa-rss' => 'fa-rss', 'fa-rss-square' => 'fa-rss-square', 'fa-rub' => 'fa-rub', 'fa-scissors' => 'fa-scissors', 'fa-search' => 'fa-search', 'fa-search-minus' => 'fa-search-minus', 'fa-search-plus' => 'fa-search-plus', 'fa-share' => 'fa-share', 'fa-share-square' => 'fa-share-square', 'fa-share-square-o' => 'fa-share-square-o', 'fa-shield' => 'fa-shield', 'fa-shopping-cart' => 'fa-shopping-cart', 'fa-sign-in' => 'fa-sign-in', 'fa-sign-out' => 'fa-sign-out', 'fa-signal' => 'fa-signal', 'fa-sitemap' => 'fa-sitemap', 'fa-skype' => 'fa-skype', 'fa-smile-o' => 'fa-smile-o', 'fa-sort' => 'fa-sort', 'fa-sort-alpha-asc' => 'fa-sort-alpha-asc', 'fa-sort-alpha-desc' => 'fa-sort-alpha-desc', 'fa-sort-amount-asc' => 'fa-sort-amount-asc', 'fa-sort-amount-desc' => 'fa-sort-amount-desc', 'fa-sort-asc' => 'fa-sort-asc', 'fa-sort-desc' => 'fa-sort-desc', 'fa-sort-numeric-asc' => 'fa-sort-numeric-asc', 'fa-sort-numeric-desc' => 'fa-sort-numeric-desc', 'fa-spinner' => 'fa-spinner', 'fa-square' => 'fa-square', 'fa-square-o' => 'fa-square-o', 'fa-stack-exchange' => 'fa-stack-exchange', 'fa-stack-overflow' => 'fa-stack-overflow', 'fa-star' => 'fa-star', 'fa-star-half' => 'fa-star-half', 'fa-star-half-o' => 'fa-star-half-o', 'fa-star-o' => 'fa-star-o', 'fa-step-backward' => 'fa-step-backward', 'fa-step-forward' => 'fa-step-forward', 'fa-stethoscope' => 'fa-stethoscope', 'fa-stop' => 'fa-stop', 'fa-strikethrough' => 'fa-strikethrough', 'fa-subscript' => 'fa-subscript', 'fa-suitcase' => 'fa-suitcase', 'fa-sun-o' => 'fa-sun-o', 'fa-superscript' => 'fa-superscript', 'fa-table' => 'fa-table', 'fa-tablet' => 'fa-tablet', 'fa-tachometer' => 'fa-tachometer', 'fa-tag' => 'fa-tag', 'fa-tags' => 'fa-tags', 'fa-tasks' => 'fa-tasks', 'fa-terminal' => 'fa-terminal', 'fa-text-height' => 'fa-text-height', 'fa-text-width' => 'fa-text-width', 'fa-th' => 'fa-th', 'fa-th-large' => 'fa-th-large', 'fa-th-list' => 'fa-th-list', 'fa-thumb-tack' => 'fa-thumb-tack', 'fa-thumbs-down' => 'fa-thumbs-down', 'fa-thumbs-o-down' => 'fa-thumbs-o-down', 'fa-thumbs-o-up' => 'fa-thumbs-o-up', 'fa-thumbs-up' => 'fa-thumbs-up', 'fa-ticket' => 'fa-ticket', 'fa-times' => 'fa-times', 'fa-times-circle' => 'fa-times-circle', 'fa-times-circle-o' => 'fa-times-circle-o', 'fa-tint' => 'fa-tint', 'fa-trash-o' => 'fa-trash-o', 'fa-trello' => 'fa-trello', 'fa-trophy' => 'fa-trophy', 'fa-truck' => 'fa-truck', 'fa-try' => 'fa-try', 'fa-tumblr' => 'fa-tumblr', 'fa-tumblr-square' => 'fa-tumblr-square', 'fa-twitter' => 'fa-twitter', 'fa-twitter-square' => 'fa-twitter-square', 'fa-umbrella' => 'fa-umbrella', 'fa-underline' => 'fa-underline', 'fa-undo' => 'fa-undo', 'fa-unlock' => 'fa-unlock', 'fa-unlock-alt' => 'fa-unlock-alt', 'fa-upload' => 'fa-upload', 'fa-usd' => 'fa-usd', 'fa-user' => 'fa-user', 'fa-user-md' => 'fa-user-md', 'fa-users' => 'fa-users', 'fa-video-camera' => 'fa-video-camera', 'fa-vimeo-square' => 'fa-vimeo-square', 'fa-vk' => 'fa-vk', 'fa-volume-down' => 'fa-volume-down', 'fa-volume-off' => 'fa-volume-off', 'fa-volume-up' => 'fa-volume-up', 'fa-weibo' => 'fa-weibo', 'fa-wheelchair' => 'fa-wheelchair', 'fa-windows' => 'fa-windows', 'fa-wrench' => 'fa-wrench', 'fa-xing' => 'fa-xing', 'fa-xing-square' => 'fa-xing-square', 'fa-youtube' => 'fa-youtube', 'fa-youtube-play' => 'fa-youtube-play', 'fa-youtube-square' => 'fa-youtube-square');
?>
<div id="c5dk_rating_form">
	<?php  echo $form->hidden("votesID", $votesID); ?>
	<?php  echo $form->checkbox("pageRating", 1, $pageRating, ""); ?> <?php  echo t("Use this block as Page Rating"); ?><br />
	<br />
	<?php  echo 'This add-on uses <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blanc">Font Awesome</a> icons for the rating representation. Please go to the website for a fully list of icons.'; ?><br />
	<br />
	<table>
		<tr>
			<td></td>
			<td><?php  echo t("Class Name"); ?></td>
			<td><?php  echo t("Size"); ?></td>
			<td><?php  echo t("Color"); ?></td>
		</tr>
		<tr>
			<th><?php  echo t("Empty Icon"); ?></th>
			<td><?php  echo $form->select("emptyChar", $faOptions, $emptyChar, array("class" => "selectpicker")); ?></td>
			<td><?php  echo $form->text("emptySize", $emptySize, array("style" => "width: 20px;")); ?><?php  echo t("px"); ?></td>

		</tr>
		<tr>
			<th><?php  echo t("Full Icon"); ?></th>
			<td><?php  echo $form->select("fullChar", $faOptions, $fullChar, array("class" => "selectpicker")); ?></td>
			<td><?php  echo $form->text("fullSize", $fullSize, array("style" => "width: 20px;")); ?><?php  echo t("px"); ?></td>

		</tr>
		<tr>
			<th><?php  echo $form->label("textLeft", t("Prefix text")); ?></th>
			<td colspan="3"><?php  echo $form->text("textLeft", $textLeft, ""); ?></td>
		</tr>
		<tr>
			<th><?php  echo $form->label("textRight", t("Suffix text")); ?></th>
			<td colspan="3"><?php  echo $form->text("textRight", $textRight, ""); ?></td>
		</tr>
		<tr>
			<td colspan="4"><?php  echo t("Note: %COUNT% = Print the voting count number."); ?></td>
		</tr>
	</table>
	<?php  echo $form->label("align", t("Align block")); ?><br />
	<?php  echo $form->radio("align", "left", $align, ""); ?> <?php  echo t("Align Left"); ?><br />
	<?php  echo $form->radio("align", "center", $align, ""); ?> <?php  echo t("Align Center"); ?><br />
	<?php  echo $form->radio("align", "right", $align, ""); ?> <?php  echo t("Align Right"); ?><br />

	<div id="c5dk_rating_preview">
		<span id="c5dk_rating_frame">
			<span class="c5dk_rating_prefix"><?php  echo $textLeft; ?></span>
			<i class="c5dk_full fa"></i><i class="c5dk_full fa"></i><i class="c5dk_full fa"></i><i class="c5dk_empty fa"></i><i class="c5dk_empty fa"></i>
			<span class="c5dk_rating_surfix"><?php  echo $textRight; ?></span>
		</span>
	</div>
</div>

<script style="text/javascript">
if(!c5dk){ var c5dk = {} };
if(!c5dk.rating){ c5dk.rating = {} };
if(!c5dk.rating.preview){ c5dk.rating.preview = {
	update:function (){
		if($("#c5dk_rating_form #textLeft").val()){ $("#c5dk_rating_preview .c5dk_rating_prefix").html($("#c5dk_rating_form #textLeft").val()); }
		if($("#c5dk_rating_form #textRight").val()){ $("#c5dk_rating_preview .c5dk_rating_surfix").html($("#c5dk_rating_form #textRight").val()); }
		$("#c5dk_rating_preview .c5dk_full").removeClass().addClass('c5dk_full fa ' + $("#c5dk_rating_form #fullChar").val())
			.css("font-size", $("#c5dk_rating_form #fullSize").val() + "px")
			.css("color", $("#c5dk_rating_form #fullColor").val());
		$("#c5dk_rating_preview .c5dk_empty").removeClass().addClass('c5dk_empty fa ' + $("#c5dk_rating_form #emptyChar").val())
			.css("font-size", $("#c5dk_rating_form #emptySize").val() + "px")
			.css("color", $("#c5dk_rating_form #emptyColor").val());
		var alignVal = $('#c5dk_rating_form input[name=align]:checked').val();
		if(alignVal == "left" || alignVal == "right"){
			$('#c5dk_rating_preview').css('text-align', '');
			$('#c5dk_rating_preview span#c5dk_rating_frame').css('float', alignVal);
		} else if(alignVal == "center"){
			$('#c5dk_rating_preview').css('text-align', 'center');
			$('#c5dk_rating_preview span#c5dk_rating_frame').css('float', '');
		}
	}
}};

$(document).ready(function() {
	
	$("#c5dk_rating_form #fullChar, #c5dk_rating_form #emptyChar, #c5dk_rating_form #fullSize, #c5dk_rating_form #emptySize, #c5dk_rating_form #fullColor, #c5dk_rating_form #emptyColor, #c5dk_rating_form #textLeft, #c5dk_rating_form #textRight, #c5dk_rating_form input[name=align]").change(function(event) {
		c5dk.rating.preview.update();
	});
	c5dk.rating.preview.update();
});
</script>

<style type="text/css">
	#c5dk_rating_form table { margin-bottom: 0; }
	#c5dk_rating_form table th{ margin-bottom: 10px; height: 36px; }
	#c5dk_rating_form table td{ padding: 0 10px; }
	#c5dk_rating_form div.ccm-color-swatch-wrapper{ top: -8px; }
	#c5dk_rating_preview{ margin-top: 10px;}
	#c5dk_rating_preview i{ width: 20px; }
</style>