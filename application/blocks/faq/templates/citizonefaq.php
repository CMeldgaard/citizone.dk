<?php defined('C5_EXECUTE') or die("Access Denied.");
$faqCount = 1;
?>
<div class="ccm-faq-container">
    <?php if (count($rows) > 0) { ?>
		<?php foreach ($rows as $row) { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $faqCount ?>" class="collapsed"><?php echo $row['linkTitle'] ?></a></h4>
			</div>
			<div class="panel-collapse collapse" id="collapse-<?php echo $faqCount ?>">
				<div class="panel-body">
					<p><?php echo $row['description'] ?></p>
				</div>
			</div>
		</div>
		<?php $faqCount++;
            } ?>
		
    <?php } else { ?>
            <p><?php echo t('Der er ikke nogen FAQ endnu.'); ?></p>
    <?php } ?>
</div>
