<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
?>

<?php if ($pageListTitle): ?>
	<div class="ccm-block-page-list-header">
		<h3><?php echo $pageListTitle?></h3>
	</div>
<?php endif; ?>

<div class="row product-row">

    <?php foreach ($pages as $page):

		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
		$target = empty($target) ? '_self' : $target;
		$thumbnail = $page->getAttribute('thumbnail');
        $hoverLinkText = $title;
        $description = $page->getCollectionDescription();
        $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
        $description = $th->entities($description);
        if ($useButtonForLink) {
            $hoverLinkText = $buttonLinkText;
        }

        ?>
<a class="col-sm-6 col-md-3 mb20" href="<?php echo $url ?>">
	<div class="product-thumb">
		<header class="product-header">
			<?php
                $img = Core::make('html/image', array($thumbnail));
                $tag = $img->getTag();
                $tag->addClass('img-responsive');
                print $tag;
                ?>
		</header>
		<div class="product-inner">
			<h5 class="product-title"><?php echo $title; ?></h5>
			<?php if ($includeDescription): ?>
			<p class="product-desciption"><?php echo $description ?></p>
			 <?php endif; ?>
			<p class="product-link"> Læs mere</p>
		</div>
	</div>
</a>
	<?php endforeach; ?>

    <?php if (count($pages) == 0): ?>
        <div class="ccm-block-page-list-no-pages"><?php echo $noResultsMessage?></div>
    <?php endif;?>

</div>

<?php if ( $c->isEditMode() && $controller->isBlockEmpty()): ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php endif; ?>